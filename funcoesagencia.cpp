#include "funcoesagencia.h"

agencia::~agencia()
{
}

agencia::agencia()
{
}

agencia::agencia(string banco, string endereco, string cidade, string bairro, string estado, int cep, int telefone)
{
    this->banco = banco;
    this->endereco = endereco;
    this->cidade = cidade;
    this->bairro = bairro;
    this->estado = estado;
    this->cep = cep;
    this->telefone = telefone;    
}    

string agencia::getbanco()
{
    return banco;
} 

void agencia::setbanco(string banco)
{
    this->banco = banco;
}        

string agencia::getendereco()
{
    return endereco;
}    

void agencia::setendereco(string endereco)
{
    this->endereco = endereco;
}
    
string agencia::getcidade()
{
    return cidade;
}    

void agencia::setcidade(string cidade)
{
    this->cidade = cidade;
}    

string agencia::getbairro()
{
    return bairro;
}    

void agencia::setbairro(string bairro)
{
    this->bairro = bairro;
}    

string agencia::getestado()
{
    return estado;
}    

void agencia::setestado(string estado)
{
    this->estado = estado;    
}   
 
int agencia::getcep()
{
    return cep;   
}  

void agencia::setcep(int cep)  
{
    this->cep = cep;
}    

int agencia::gettelefone()
{
    return telefone;
}    

void agencia::settelefone(int telefone)
{
    this->telefone = telefone;
}    

