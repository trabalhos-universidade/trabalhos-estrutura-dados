#include <iostream>
#include <cstdlib>
#include <string>
#include <map>

using namespace std;

class conta
{
    private:
        string tipo;
        int numero_conta;
        int cliente;
        int agencia;
        string data_cadastro;
        string atendente;
        
    public:
        conta();
        conta(string tipo, int numero_conta, int cliente, int agencia, string data_cadastro, string atendente);
        ~conta();
        string gettipo();
        void settipo(string tipo);
        int getnumero_conta();
        void setnumero_conta(int numero_conta);
        int getcliente();
        void setcliente(int cliente);
        int getagencia();
        void setagencia(int agencia);
        string getdata_cadastro();
        void setdata_cadastro(string data_cadastro);
        string getatendente();
        void setatendente(string atendente);        
}    
